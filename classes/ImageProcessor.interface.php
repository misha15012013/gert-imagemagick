<?php


interface ImageGenerator {
  
  public function set_mask_file($mask_file);
  
  public function set_texture_location($texture_location);
  
  public function set_engraving_text($text);
  
  public function set_texture_mask_file($texture_mask_file);
  
  public function set_engraving_file($engraving_file);
  
  public function set_hoop_file($hoop_file);
  
  public function set_main_backgound_image($bg_file);
  
  public function process_mask($tmp_custom_file);
  
  public function put_mask_to_chain();
  
  public function engrave();
  
  public function add_hoop();
  
  public function get_image();
  
  public function get_custom_image();
}
