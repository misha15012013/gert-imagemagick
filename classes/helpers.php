<?php
/**
 * Created by PhpStorm.
 * User: sbushinsky
 * Date: 09.08.17
 * Time: 12:52
 */

function generate_text_image($text) {
  $tmp_file_name = $this->tmp_directory . "/" . uniqid() . ".png";
  $cmd = CONVERT . ' -size 200x200 -background transparent -font ' . ASSETS_DIR . '/font.ttf -pointsize 40 -fill rgba\(255,255,255,0.35\) -gravity Center caption:"' . $text . '" -flatten ' . $tmp_file_name;
  exec($cmd);
  
  if(file_exists($tmp_file_name) && filesize($tmp_file_name)){
    return $tmp_file_name;
  }
  echo $cmd;die;
  return false;
}