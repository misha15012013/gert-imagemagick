<?php
/**
 * Created by PhpStorm.
 * User: sbushinskii
 * Date: 8/4/17
 * Time: 11:21 PM
 */
require_once 'ImageProcessor.interface.php';

class ImageProcessorCat implements ImageGenerator{
  /**
   * @var Imagick object
   */
  private $image = FALSE;
  
  /**
   * @var Scale parameter
   */
  private $scale = 1.5;

  private $main_bg_image_file;
  private $custom_mask_file;
  private $texture_location;
  private $texture_mask_file;
  private $engraving_file;
  private $hoop_file;
  private $tmp_directory;
  private $engraving_text;
  
  private $custom_image;
  private $tmp_custom_file;
  
  /**
   * @param mixed $mask_file
   */
  public function set_mask_file($mask_file) {
    $this->custom_mask_file = $mask_file;
  }
  
  public function set_engraving_text($text){
    $this->engraving_text = $text;
    $new_file = $this->generate_text_image($text);
    $this->engraving_file = $new_file;
  }
  
  /**
   * @param mixed $texture_location
   */
  public function set_texture_location($texture_location) {
    $this->texture_location = $texture_location;
  }
  
  /**
   * @param mixed $texture_mask_file
   */
  public function set_texture_mask_file($texture_mask_file) {
    $this->texture_mask_file = $texture_mask_file;
  }
  
  /**
   * @param mixed $engraving_file
   */
  public function set_engraving_file($engraving_file) {
    $this->engraving_file = $engraving_file;
  }
  
  /**
   * @param mixed $hoop_file
   */
  public function set_hoop_file($hoop_file) {
    $this->hoop_file = $hoop_file;
  }
  
  /**
   * ImageProcessor constructor.
   * @param $tmp_directory string
   */
  public function __construct($tmp_directory) {
    if (!is_dir($tmp_directory)) {
      @mkdir($tmp_directory);
    }
    
    if (!is_dir($tmp_directory) || !is_writable($tmp_directory)) {
      exit('TMP dir is not exists or it\'s not writable');
    }
    $this->tmp_directory = $tmp_directory;
  }
  
  /**
   * Generate new mask image with given texture.
   *
   * @return bool
   */
  public function process_mask($tmp_custom_file) {
    
    $gradient_start_color = 'gray';
    $gradient_end_color = 'white';
    
    $generate_gradient_cmd_format = CONVERT . ' -size 1x1000 gradient: -gamma 0.9 -function Sinusoid 2.25,0,0.5,0.5 \( gradient:%s-%s -gamma 1 \) +swap -compose Overlay -composite -rotate 90 %s';
    $gradient_file = $this->tmp_directory . '/gradient_' . uniqid() . '.png';
    exec(sprintf($generate_gradient_cmd_format, $gradient_start_color, $gradient_end_color, $gradient_file));
    if(file_exists($gradient_file) && filesize($gradient_file)) {
      
    } else {
      exit("Failed to generate gradient file");
    }
  
    $cmd_format = CONVERT . ' -size 524x292 gradient:white-black \( %s -negate \) -compose CopyOpacity -composite \(  %s \( +clone -negate -level 0%%x10%% \) -compose CopyOpacity -composite  -auto-gamma -auto-level \) -compose Overlay -composite %s  -clut \( +clone -background gray -shadow 80x2+3+3 -write '.$this->tmp_directory.'/'.uniqid().'_metallic_shad.png \) +swap -compose Over -composite -trim +repage %s';
    $cmd = sprintf($cmd_format, $this->custom_mask_file, $this->custom_mask_file, $gradient_file, $tmp_custom_file);
    exec($cmd);
    exec(CONVERT . ' ' . $tmp_custom_file . ' -resize 350x ' . $tmp_custom_file);
    if (file_exists($tmp_custom_file) && filesize($tmp_custom_file)) {
      $this->tmp_custom_file = $tmp_custom_file;
      // TODO remove it later
      $tmp_image = new Imagick();
      $tmp_image->readImage($tmp_custom_file);
      $this->custom_image = $tmp_image;
      return TRUE;
    }
    echo $cmd;
    
    exit('Mask file wasn\'t processed');
    //return FALSE;
  }
  
  public function set_main_backgound_image($bg_image_file){
    $this->main_bg_image_file = $bg_image_file;
//    $image = new Imagick();
//    $image->readImage($this->main_bg_image_file);
//    $this->custom_image = $image;
  }
  
  /**
   * Composize processed mask with texture to the main image (chain).
   * @param $this->texture_mask_file
   */
  public function put_mask_to_chain($offset_left = FALSE, $offset_top = FALSE) {
    $size = 200;
    
    $image = new Imagick();
    $image->readImage($this->main_bg_image_file);

    $render = $this->custom_image;
//    header('Content-Type: image/png');
//    echo $render;
//    die;
    //$render->readImage($this->tmp_custom_file);
    
    $this->scale = $size / max($render->getImageWidth(), $render->getImageHeight());
    
    $render->setImageFormat("png");
    $render->resizeImage($this->scale * $render->getImageWidth(), $this->scale * $render->getImageHeight(), Imagick::FILTER_LANCZOS, 1);
    
    $shadow = clone $render;
    
    $shadow->setImageBackgroundColor(new ImagickPixel('black'));
    $shadow->shadowImage(80, 1, 2, 2);
    $shadow->compositeImage($render, Imagick::COMPOSITE_OVER, 0, 0);
    $render = $shadow;
    
    if(!$offset_left){
      $offset_left = 617.5 - 0.5 * $render->getImageWidth();
    }
    if(!$offset_top){
      $offset_top = 626.5 - 45;
    }
    $image->compositeImage($render, imagick::COMPOSITE_OVER, $offset_left, $offset_top);
    $this->image = $image;
  }
  
  /**
   * Composite image with engraving element.
   *
   * @param $engraving_file string
   */
  public function engrave() {
    $fill = new \ImagickPixel('rgba(196,192,180, 0.9)');
    $target = 'rgba(0,0,0, 1.0)';
    
    $engraving = new Imagick();
    $engraving->readImage($this->engraving_file);
    
    $fuzz = 0.8 * $engraving->getQuantumRange()['quantumRangeLong'];
    $engraving->opaquePaintImage($target, $fill, $fuzz, FALSE, Imagick::CHANNEL_DEFAULT);
    $engraving->transparentPaintImage('rgb(255,255,255)', 0, 0.10 * $engraving->getQuantumRange()['quantumRangeLong'], FALSE);
    
    //$engraving->motionBlurImage(0, 2, -135);
    $engraving->resizeImage(360, 340, Imagick::FILTER_LANCZOS, 1);
    $this->custom_image->compositeImage($engraving, imagick::COMPOSITE_OVER, -5 , -10);
  }
  
  
  /**
   * Composite hoop image to the main image.
   *
   * @param $hoop_file string
   */
  public function add_hoop($offset_left = FALSE, $offset_top = FALSE) {
    $hoop = new Imagick();
    $hoop->readImage($this->hoop_file);
  
    if(!$offset_left){
      $offset_left = 616 - $hoop->getImageWidth() / 2;
    }
    if(!$offset_top){
      $offset_top = 575 - $hoop->getImageHeight() / 2;
    }
    
    $this->image->compositeImage($hoop, imagick::COMPOSITE_DEFAULT, $offset_left, $offset_top);
  }
  
  /**
   * Get Imagick object.
   *
   * @return mixed
   */
  public function get_image() {
    return $this->image;
  }
  
  public function get_custom_image() {
    return $this->custom_image;
  }
  
}