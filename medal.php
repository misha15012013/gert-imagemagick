<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'classes/cat.class.php';
define('ABS_PATH', getcwd());
define('TMP_DIR', ABS_PATH . '/tmp');
define('ASSETS_DIR', ABS_PATH . '/assets');
define('CONVERT', '/usr/bin/convert');

$texture_mask_file = TMP_DIR . "/test_" . uniqid() .".png";
$mask_file = ASSETS_DIR . '/mask.png';
$texture_location = ASSETS_DIR . "/metal2.jpg";
$hoop_file = ASSETS_DIR . '/hoop.png';
$engraving_file = ASSETS_DIR . "/engraving.png";

$main_background = ASSETS_DIR . '/medal.jpg';


// Initialize image processor, check if TMP dir exists.
$cat_image_processor = new ImageProcessorCat(TMP_DIR);

/**
 * Set test caption generated in the method above
 */
$engraving_file = ASSETS_DIR . '/caption.png';
$texture_location = ASSETS_DIR . '/medal.jpg';

$custom_text = isset($_GET['text']) ? trim($_GET['text']) : "Lorem Ipsum dolor sit amet! This is one more test goes here";

$cat_image_processor->set_engraving_text($custom_text);
$cat_image_processor->set_texture_location($texture_location);

$cat_image_processor->engrave();

// Output generated image into browser.
$processed_image = $cat_image_processor->get_image();
header('Content-Type: image/png');
echo $processed_image;
?>
