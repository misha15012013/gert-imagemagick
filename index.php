<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'classes/cat.class.php';
define('ABS_PATH', getcwd());
define('TMP_DIR', ABS_PATH . '/tmp');
define('ASSETS_DIR', ABS_PATH . '/assets');
define('CONVERT', '/usr/bin/convert');

$tmp_custom_file = TMP_DIR . "/test_" . uniqid() .".png";
$custom_mask_file = ASSETS_DIR . '/mask.png';
$texture_location = ASSETS_DIR . "/metal5.jpg";
$hoop_file = ASSETS_DIR . '/hoop.png';
$engraving_file = ASSETS_DIR . "/engraving.png";
//$main_background_image = ASSETS_DIR . '/base_chain.jpg';
$main_background_image = ASSETS_DIR . '/chain-3.png';

// Initialize image processor, check if TMP dir exists.
$cat_image_processor = new ImageProcessorCat(TMP_DIR);

// Set input data.
$cat_image_processor->set_mask_file($custom_mask_file);


$cat_image_processor->set_texture_location($texture_location);
$cat_image_processor->set_engraving_file($engraving_file);
$cat_image_processor->set_hoop_file($hoop_file);
$cat_image_processor->set_main_backgound_image($main_background_image);


// Process image.
$cat_image_processor->process_mask($tmp_custom_file);
$processed_image = $cat_image_processor->get_custom_image();
//header('Content-Type: image/png');
//echo $processed_image;exit;

$cat_image_processor->engrave();

/* */
//$processed_image = $cat_image_processor->get_image();
//header('Content-Type: image/png');
//echo $processed_image;
//exit;
/* */
$offset_left = 270;
$offset_top = 600;
$cat_image_processor->put_mask_to_chain($offset_left, $offset_top);
$cat_image_processor->add_hoop($offset_left+50, $offset_top-40);

// Output generated image into browser.
$processed_image = $cat_image_processor->get_image();
header('Content-Type: image/png');
echo $processed_image;
?>
